package chess.game;

import chess.game.figures.Bishop;
import chess.game.figures.Color;
import chess.game.figures.Horse;
import chess.game.figures.King;
import chess.game.figures.Pawn;
import chess.game.figures.Queen;
import chess.game.figures.Tower;

public class GameInfo {

	private final GameField[][] boardInfo = new GameField[8][8];

	public GameInfo() {
		boardInfo[0][0] = new GameField(new Tower(Color.BLACK));
		boardInfo[0][1] = new GameField(new Horse(Color.BLACK));
		boardInfo[0][2] = new GameField(new Bishop(Color.BLACK));
		boardInfo[0][3] = new GameField(new Queen(Color.BLACK));
		boardInfo[0][4] = new GameField(new King(Color.BLACK));
		boardInfo[0][5] = new GameField(new Bishop(Color.BLACK));
		boardInfo[0][6] = new GameField(new Horse(Color.BLACK));
		boardInfo[0][7] = new GameField(new Tower(Color.BLACK));
		for (int i = 0; i < 8; i++) {
			boardInfo[1][i] = new GameField(new Pawn(Color.BLACK));
		}
		boardInfo[7][0] = new GameField(new Tower(Color.WHITE));
		boardInfo[7][1] = new GameField(new Horse(Color.WHITE));
		boardInfo[7][2] = new GameField(new Bishop(Color.WHITE));
		boardInfo[7][3] = new GameField(new Queen(Color.WHITE));
		boardInfo[7][4] = new GameField(new King(Color.WHITE));
		boardInfo[7][5] = new GameField(new Bishop(Color.WHITE));
		boardInfo[7][6] = new GameField(new Horse(Color.WHITE));
		boardInfo[7][7] = new GameField(new Tower(Color.WHITE));
		for (int i = 0; i < 8; i++) {
			boardInfo[6][i] = new GameField(new Pawn(Color.WHITE));
		}

		for (int i = 2; i < 6; i++) {
			for (int j = 0; j < 8; j++) {
				boardInfo[i][j] = new GameField();
			}
		}

	}
	
	public GameField[][] getBoardInfo() {
		return boardInfo;
		
	}

}