package chess.game;

import chess.game.figures.Figure;

public class GameField {
	private Figure figure = null;

	public GameField() {
		super();
	}
	
	public GameField(Figure figure) {
		super();
		this.figure = figure;
	}

	public boolean isEmpty() {
		if (figure == null) {
			return true;
		}
		return false;
		// return figure == null;
	}

	public Figure getFigure() {
		return figure;
	}

	public void moveTo(Figure figure) {
		this.figure = figure;
	}

	public void moveFrom() {
		this.figure = null;
	}

}
