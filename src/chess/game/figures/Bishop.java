package chess.game.figures;

import java.util.ArrayList;
import java.util.List;

import chess.game.GameField;

public class Bishop extends Figure {

	public Bishop(Color color) {
		super(color);
	}

	@Override
	public List<GameField> showValidMoves(GameField[][] tab, int x, int y) {
		List<GameField> bishopValidMoves = new ArrayList<GameField>();
		Figure.Site.NE.addIfValid(bishopValidMoves, x, y, tab, this);
		Figure.Site.SE.addIfValid(bishopValidMoves, x, y, tab, this);
		Figure.Site.SW.addIfValid(bishopValidMoves, x, y, tab, this);
		Figure.Site.NW.addIfValid(bishopValidMoves, x, y, tab, this);
		return bishopValidMoves;
	}

}
