package chess.game.figures;

import java.util.ArrayList;
import java.util.List;

import chess.game.GameField;

public class King extends Figure {

	public King(Color color) {
		super(color);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<GameField> showValidMoves(GameField[][] tab, int x, int y) {
		List<GameField> kingValidMoves = new ArrayList<GameField>();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				int szukanyX = x + i - 1;
				int szukanyY = y + j - 1;
				if (isValidXY(szukanyX, szukanyY)
						&& (tab[szukanyX][szukanyY].isEmpty() || this.color != tab[szukanyX][szukanyY].getFigure().getColor())) {
					kingValidMoves.add(tab[szukanyX][szukanyY]);
				}
			}
		}

		return kingValidMoves;
	}

	
}
