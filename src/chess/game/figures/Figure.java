package chess.game.figures;

import java.util.List;

import chess.game.GameField;

public abstract class Figure {
	protected final Color color;

	public Figure(Color color) {
		this.color = color;
	}

	public abstract List<GameField> showValidMoves(GameField[][] tab, int x, int y);

	protected boolean isValidXY(int szukanyX, int szukanyY) {
		if (szukanyX >= 0 && szukanyX <= 7 && szukanyY >= 0 && szukanyY <= 7) {
			return true;
		}
		return false;
	}

	protected void addIfValidPlace(List<GameField> list, int indexX, int indexY, GameField[][] tab) {
		if (isValidXY(indexX, indexY)) {
			list.add(tab[indexX][indexY]);
		}

	}

	protected boolean isEmptyAndDifColor(GameField[][] tab, int x, int y) {
		if (tab[x][y].isEmpty() || !(this.color == tab[x][y].getFigure().getColor())) {
			return true;
		}
		return false;

	}

	public void move(GameField from, GameField to) {
		from.moveFrom();
		to.moveTo(this);
	}

	public Color getColor() {
		return color;
	}

	protected static enum Site {
		N {
			@Override
			protected void addIfValidWithSite(List<GameField> list, int x, int y, GameField[][] tab, Figure figure, int i) {
				if (figure.isEmptyAndDifColor(tab, x, y + i)) {
					figure.addIfValidPlace(list, x, y + i, tab);
				}
			}
		},
		E {
			@Override
			protected void addIfValidWithSite(List<GameField> list, int x, int y, GameField[][] tab, Figure figure, int i) {
				if (figure.isEmptyAndDifColor(tab, x + i, y)) {
					figure.addIfValidPlace(list, x + i, y, tab);
				}
			}
		},
		S {
			@Override
			protected void addIfValidWithSite(List<GameField> list, int x, int y, GameField[][] tab, Figure figure, int i) {
				if (figure.isEmptyAndDifColor(tab, x, y - i)) {
					figure.addIfValidPlace(list, x, y - i, tab);
				}
			}
		},
		W {
			@Override
			protected void addIfValidWithSite(List<GameField> list, int x, int y, GameField[][] tab, Figure figure, int i) {
				if (figure.isEmptyAndDifColor(tab, x - i, y)) {
					figure.addIfValidPlace(list, x - i, y, tab);
				}
			}
		},
		NE {
			@Override
			protected void addIfValidWithSite(List<GameField> list, int x, int y, GameField[][] tab, Figure figure, int i) {
				if (figure.isEmptyAndDifColor(tab, x + i, y + i)) {
					figure.addIfValidPlace(list, x + i, y + i, tab);
				}
			}
		},
		SE {
			@Override
			protected void addIfValidWithSite(List<GameField> list, int x, int y, GameField[][] tab, Figure figure, int i) {
				if (figure.isEmptyAndDifColor(tab, x + i, y - i)) {
					figure.addIfValidPlace(list, x + i, y - i, tab);
				}
			}
		},
		SW {
			@Override
			protected void addIfValidWithSite(List<GameField> list, int x, int y, GameField[][] tab, Figure figure, int i) {
				if (figure.isEmptyAndDifColor(tab, x - i, y - i)) {
					figure.addIfValidPlace(list, x - i, y - i, tab);
				}
			}
		},
		NW {
			@Override
			protected void addIfValidWithSite(List<GameField> list, int x, int y, GameField[][] tab, Figure figure, int i) {
				if (figure.isEmptyAndDifColor(tab, x - i, y + i)) {
					figure.addIfValidPlace(list, x - i, y + i, tab);
				}
			}
		};

		protected abstract void addIfValidWithSite(List<GameField> list, int x, int y, GameField[][] tab, Figure figure, int i);
		
		public void addIfValid(List<GameField> list, int x, int y, GameField[][] tab, Figure figure) {
			for (int i = 1; i < 8; i++) {
				this.addIfValidWithSite(list, x, y, tab, figure, i);
			}
		}

	}

}
