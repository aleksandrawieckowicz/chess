package chess.game.figures;

import java.util.ArrayList;
import java.util.List;

import chess.game.GameField;

public class Horse extends Figure {

	public Horse(Color color) {
		super(color);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public List<GameField> showValidMoves(GameField[][] tab, int x, int y) {
		List<GameField> horseValidMoves = new ArrayList<GameField>();
		addIfValidPlace(horseValidMoves,x - 1, y + 2, tab);
		addIfValidPlace(horseValidMoves,x + 1, y + 2, tab); 
		addIfValidPlace(horseValidMoves,x + 2, y + 1, tab);
		addIfValidPlace(horseValidMoves,x + 2, y - 1, tab);
		addIfValidPlace(horseValidMoves,x + 1, y - 2, tab);
		addIfValidPlace(horseValidMoves,x - 1, y - 2, tab);
		addIfValidPlace(horseValidMoves,x - 2, y - 1, tab);
		addIfValidPlace(horseValidMoves,x - 2, y + 1, tab);
		
		horseValidMoves.removeIf((element) -> !element.isEmpty() && this.color == element.getFigure().getColor());
		
		return horseValidMoves;
	}

}
