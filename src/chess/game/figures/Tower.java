package chess.game.figures;

import java.util.ArrayList;
import java.util.List;

import chess.game.GameField;

public class Tower extends Figure {

	public Tower(Color color) {
		super(color);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<GameField> showValidMoves(GameField[][] tab, int x, int y) {
		List<GameField> towerValidMoves = new ArrayList<GameField>();

		Figure.Site.N.addIfValid(towerValidMoves, x, y, tab, this);
		Figure.Site.E.addIfValid(towerValidMoves, x, y, tab, this);
		Figure.Site.S.addIfValid(towerValidMoves, x, y, tab, this);
		Figure.Site.W.addIfValid(towerValidMoves, x, y, tab, this);
		

		return towerValidMoves;

	}

}
