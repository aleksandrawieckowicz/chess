package chess.game.figures;

import java.util.ArrayList;
import java.util.List;

import chess.game.GameField;

public class Pawn extends Figure {

	private boolean firstMove = true;

	public Pawn(Color color) {
		super(color);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<GameField> showValidMoves(GameField[][] tab, int x, int y) {
		List<GameField> pawnValidMoves = new ArrayList<GameField>();

		if (this.color == Color.WHITE) {
			if (this.firstMove == true) {
				addIfValidPlace(pawnValidMoves, x + 2, y, tab);
				addIfValidPlace(pawnValidMoves, x + 1, y, tab);
			} else
				addIfValidPlace(pawnValidMoves, x + 1, y, tab);

		} else if (this.color == Color.BLACK)
			if (this.firstMove == true) {
				addIfValidPlace(pawnValidMoves, x - 2, y, tab);
				addIfValidPlace(pawnValidMoves, x - 1, y, tab);
			} else
				addIfValidPlace(pawnValidMoves, x - 1, y, tab);

		pawnValidMoves.removeIf((element) -> !element.isEmpty() && this.color == element.getFigure().getColor());
		return pawnValidMoves;
	}

	@Override
	public void move(GameField from, GameField to) {
		this.firstMove = false;
		super.move(from, to);
	}

}
