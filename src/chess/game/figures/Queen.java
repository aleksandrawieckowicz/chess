package chess.game.figures;

import java.util.ArrayList;
import java.util.List;

import chess.game.GameField;

public class Queen extends Figure {

	public Queen(Color color) {
		super(color);
	}

	@Override
	public List<GameField> showValidMoves(GameField[][] tab, int x, int y) {
		List<GameField> queenValidMoves = new ArrayList<GameField>();
		Figure.Site.N.addIfValid(queenValidMoves, x, y, tab, this);
		Figure.Site.S.addIfValid(queenValidMoves, x, y, tab, this);
		Figure.Site.W.addIfValid(queenValidMoves, x, y, tab, this);
		Figure.Site.E.addIfValid(queenValidMoves, x, y, tab, this);
		Figure.Site.NE.addIfValid(queenValidMoves, x, y, tab, this);
		Figure.Site.SE.addIfValid(queenValidMoves, x, y, tab, this);
		Figure.Site.SW.addIfValid(queenValidMoves, x, y, tab, this);
		Figure.Site.NW.addIfValid(queenValidMoves, x, y, tab, this);
		return queenValidMoves;
	}
	

}
