package chess.gui;

import chess.game.figures.Bishop;
import chess.game.figures.Color;
import chess.game.figures.Figure;
import chess.game.figures.Horse;
import chess.game.figures.King;
import chess.game.figures.Pawn;
import chess.game.figures.Queen;
import chess.game.figures.Tower;
import javafx.scene.image.Image;

public enum Figures {
	BLACK_TOWER(Tower.class, Color.BLACK) {
		@Override
		public Image getImage() {
			return Images.blackTower;
		}
	},
	BLACK_BISHOP(Bishop.class, Color.BLACK) {
		@Override
		public Image getImage() {
			return Images.blackBishop;
		}
	},
	BLACK_HORSE(Horse.class, Color.BLACK) {
		@Override
		public Image getImage() {
			return Images.blackKnight;
		}
	},
	BLACK_KING(King.class, Color.BLACK) {
		@Override
		public Image getImage() {
			return Images.blackKing;
		}
	},
	BLACK_PAWN(Pawn.class, Color.BLACK) {
		@Override
		public Image getImage() {
			return Images.blackPawn;
		}
	},
	BLACK_QUEEN(Queen.class, Color.BLACK) {
		@Override
		public Image getImage() {
			return Images.blackQueen;
		}
	},

	WHITE_TOWER(Tower.class, Color.WHITE) {
		@Override
		public Image getImage() {
			return Images.whiteTower;
		}
	},
	WHITE_BISHOP(Bishop.class, Color.WHITE) {
		@Override
		public Image getImage() {
			return Images.whiteBishop;
		}
	},
	WHITE_HORSE(Horse.class, Color.WHITE) {
		@Override
		public Image getImage() {
			return Images.whiteKnight;
		}
	},
	WHITE_KING(King.class, Color.WHITE) {
		@Override
		public Image getImage() {
			return Images.whiteKing;
		}
	},
	WHITE_PAWN(Pawn.class, Color.WHITE) {
		@Override
		public Image getImage() {
			return Images.whitePawn;
		}
	},
	WHITE_QUEEN(Queen.class, Color.WHITE) {
		@Override
		public Image getImage() {
			return Images.whiteQueen;
		}
	};

	Figures(Class<? extends Figure> tClass, Color color) {
		this.tClass = tClass;
		this.color = color;
	}

	private Class<? extends Figure> tClass;
	private Color color;

	public static Figures valueOf(Class<? extends Figure> c, Color color) {
		for (Figures figure : Figures.values()) {
			if (figure.tClass.equals(c) && figure.color.equals(color)) {
				return figure;
			}
		}
		return null;
	}

	public abstract Image getImage();
}
