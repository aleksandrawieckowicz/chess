package chess.gui;

import javafx.scene.image.Image;

public class Images {

	public static final Image blackTower = new Image("chess/grafiki/tower_b.png", 90, 90, false, false);
	public static final Image blackKing = new Image("chess/grafiki/knight_b.png", 90, 90, false, false);
	public static final Image blackKnight = new Image("chess/grafiki/knight_b.png", 90, 90, false, false);
	public static final Image blackBishop = new Image("chess/grafiki/bishop_b.png", 90, 90, false, false);
	public static final Image blackQueen = new Image("chess/grafiki/queen_b.png", 90, 90, false, false);
	public static final Image blackPawn = new Image("chess/grafiki/pawn_b.png", 90, 90, false, false);
	public static final Image whiteTower = new Image("chess/grafiki/tower_r.png", 90, 90, false, false);
	public static final Image whiteKing = new Image("chess/grafiki/king_r.png", 90, 90, false, false);
	public static final Image whiteKnight = new Image("chess/grafiki/knight_r.png", 90, 90, false, false);
	public static final Image whiteBishop = new Image("chess/grafiki/bishop_r.png", 90, 90, false, false);
	public static final Image whiteQueen = new Image("chess/grafiki/queen_r.png", 90, 90, false, false);
	public static final Image whitePawn = new Image("chess/grafiki/pawn_r.png", 90, 90, false, false);

}
