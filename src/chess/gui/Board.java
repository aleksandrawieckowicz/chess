package chess.gui;

import chess.game.GameField;
import chess.game.GameInfo;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Paint;

public class Board extends GridPane {

	private GameInfo gameInfo = new GameInfo();

	public Board(int x, int y) {

		for (int i = 0; i < x; i++) {
			int counter = 0;
			for (int j = 0; j < y; j++) {

				Field pane = new Field();
				pane.setPrefSize(80, 80);
				this.add(pane, i, j);
				if ((counter % 2 != 0 && i % 2 == 0 && j % 2 != 0) || (counter % 2 == 0 && i % 2 != 0 && j % 2 == 0)) {
					pane.setBackground(new Background(
							new BackgroundFill(Paint.valueOf("white"), new CornerRadii(0), new Insets(0))));
				} else {
					pane.setBackground(new Background(
							new BackgroundFill(Paint.valueOf("black"), new CornerRadii(0), new Insets(0))));
				}
				counter++;
			}

		}
		showBoard();
	}

	public void showBoard() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (!gameInfo.getBoardInfo()[i][j].isEmpty()) {
					Image img = Figures.valueOf(gameInfo.getBoardInfo()[i][j].getFigure().getClass(),
							gameInfo.getBoardInfo()[i][j].getFigure().getColor()).getImage();
					this.getNodeByRowColumnIndex(i, j)
							.setBackground(new Background(new BackgroundImage(img, BackgroundRepeat.NO_REPEAT,
									BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
				}
			}
		}
	
	}

	/**
	 * zwraca jeden panel na podstawie x i y
	 * @param row
	 * @param column
	 * @return jeden panel
	 */
	private Field getNodeByRowColumnIndex(final int row, final int column) {
		Field result = null;
		ObservableList<Node> childrens = this.getChildren();

		for (Node node : childrens) {
			if (GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column) {
				result = (Field) node;
				break;
			}
		}

		return result;
	}

}
