package chess;

import chess.gui.Board;
import chess.gui.Images;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		GridPane gridPane = new Board(8, 8);

		primaryStage.setTitle("Szachy");
		primaryStage.setScene(new Scene(gridPane, 600, 600));
		primaryStage.show();

	}

}
